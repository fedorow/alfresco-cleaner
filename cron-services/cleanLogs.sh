#!/bin/ash
# There are actual absolute paths of the logs directoryes
ALFRESCO_LOGS_PATH=/logs/alfresco/
SHARE_LOGS_PATH=/logs/share/
POSTGRES_LOGS_PATH=/logs/postgres/

# Clean Alfresco logs
if [[ -d "${ALFRESCO_LOGS_PATH}" && ! -L "${ALFRESCO_LOGS_PATH}" ]] ; then
    # Remove all files from tomcat/logs older than ALFRESCO_LOGS_DAYS days
    find $ALFRESCO_LOGS_PATH -type f -mtime +$ALFRESCO_LOGS_DAYS -name "*.txt"| xargs rm 2> /dev/null
    find $ALFRESCO_LOGS_PATH -type f -mtime +$ALFRESCO_LOGS_DAYS -name "*.log"| xargs rm 2> /dev/null
    find $ALFRESCO_LOGS_PATH -type f -mtime +$ALFRESCO_LOGS_DAYS -name "*.log.*"| xargs rm 2> /dev/null
fi

# Clean Share logs
if [[ -d "${SHARE_LOGS_PATH}" && ! -L "${SHARE_LOGS_PATH}" ]] ; then
    # Remove all files from tomcat/logs older than SHARE_LOGS_DAYS days
    find $SHARE_LOGS_PATH -type f -mtime +$SHARE_LOGS_DAYS -name "*.txt"| xargs rm 2> /dev/null
    find $SHARE_LOGS_PATH -type f -mtime +$SHARE_LOGS_DAYS -name "*.log"| xargs rm 2> /dev/null
fi

# Clean Postgres logs
if [[ -d "${POSTGRES_LOGS_PATH}" && ! -L "${POSTGRES_LOGS_PATH}" ]] ; then
    # Remove all files from tomcat/logs older than POSTGRES_LOGS_DAYS days
    find $POSTGRES_LOGS_PATH -type f -mtime +$POSTGRES_LOGS_DAYS -name "*.txt"| xargs rm 2> /dev/null
    find $POSTGRES_LOGS_PATH -type f -mtime +$POSTGRES_LOGS_DAYS -name "*.log"| xargs rm 2> /dev/null
fi

# Log cleaner to the alfresco.log file
if $LOGS ; then
    echo "$(date +'%Y-%m-%d %H:%M:%S,000') INFO [Alfresco-cron-services] Log files older then: alfresco - $ALFRESCO_LOGS_DAYS days, share - $SHARE_LOGS_DAYS days, postgres - $POSTGRES_LOGS_DAYS days was removed." >> /logs/alfresco/alfresco.log
fi
